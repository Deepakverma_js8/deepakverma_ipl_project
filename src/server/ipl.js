const { matches_arr, deliveries_arr } = require('./index');
const fs = require('fs');

// Number of matches played per year for all the years in IPL
function match_played_per_year() {
    let match_per_year = {};
    matches_arr.map(match => {

        if (match_per_year[match['season']] === undefined) {
            match_per_year[match['season']] = 0;
        }
        match_per_year[match['season']] += 1;

    });
    const data = JSON.stringify(match_per_year);
    const addr = 'src/public/output/matchesPerYear.json';
    write_file(addr, data);
}
// Number of matches won per team per year in IPL.
function match_won_per_team_per_year() {
    let matches_won_per_team_obj = {};
    matches_arr.map(match => {
        if (match['winner'] != '' && matches_won_per_team_obj[match['winner']] === undefined) {
            matches_won_per_team_obj[match['winner']] = {};
        }
        if (match['winner'] != '' && matches_won_per_team_obj[match['winner']][match['season']] === undefined) {
            matches_won_per_team_obj[match['winner']][match['season']] = 0;
        }
        if (match['winner'] != '') {
            matches_won_per_team_obj[match['winner']][match['season']] += 1;
        }
    })
    const data = JSON.stringify(matches_won_per_team_obj);
    const addr = 'src/public/output/matchesPerYear.json';
    write_file(addr, data);
}
/// Extra runs conceded per team in the year 2016

function extra_runs() {
    let extra_runs_obj = {};
    matches_arr.map(match => {
        if (match['season'] === '2016') {
            deliveries_arr.map(delivery => {
                if (delivery['match_id'] === match['id']) {
                    if (extra_runs_obj[delivery['batting_team']] === undefined) {
                        extra_runs_obj[delivery['batting_team']] = 0;
                    }

                    extra_runs_obj[delivery['batting_team']] += parseInt(delivery['extra_runs']);
                }
            })
        }
    })
    const addr = "src/public/output/extraRunsPerTeam.json";
    const data = JSON.stringify(extra_runs_obj)
    write_file(addr, data);

}

// Top 10 economical bowlers in the year 2015

function economical_bowler_2015() {
    let obj = {};
    matches_arr.map(match => {
        if (match['season'] === '2015') {
            deliveries_arr.map(delivery => {
                if (delivery['match_id'] === match['id']) {
                    if (obj[delivery['bowler']] === undefined) {
                        obj[delivery['bowler']] = { 'no_of_ball': 0, 'runs_given': 0 }
                    }

                    if (delivery['wide_runs'] && delivery['noball_runs'])
                        obj[delivery['bowler']]['no_of_ball'] += 1;
                    obj[delivery['bowler']]['runs_given'] += parseInt(delivery['total_runs']);

                }

            })
        }
    })


    let economy_arr = [];

    Object.keys(obj).map(ele => {

        economy_arr.push({ 'bowler': ele, 'economy_rate': Math.round((obj[ele]['runs_given'] / (obj[ele]['no_of_ball'] / 6) * 100)) / 100 })
    })

    let top_10_economy_bowler = economy_arr.sort(function (a, b) {
        return a['economy_rate'] - b['economy_rate'];
    }).slice(0, 10);

    const addr = "src/public/output/topTenEconomicBowlers.json";
    write_file(addr, JSON.stringify(top_10_economy_bowler));

        
}
// function to write data to file
function write_file(addr, data) {
    fs.writeFile(addr, data, err => {
        if (err)
            throw (err);
    })
}
// calling function
match_played_per_year()
match_won_per_team_per_year()
extra_runs()
economical_bowler_2015()