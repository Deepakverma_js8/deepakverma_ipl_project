let csvToJson = require('convert-csv-to-json');

// Generate Array of Object in JSON format
csvToJson.fieldDelimiter(',').getJsonFromCsv("src/data/deliveries.csv");
let json = csvToJson.getJsonFromCsv("src/data/deliveries.csv");

let deliveries_arr=[]
// pushing values to get deliveries array of object
for(let i = 0 ; i < json.length ; i++){
    deliveries_arr.push(json[i]);
}

// Generate Array of Object in JSON format
csvToJson.fieldDelimiter(',').getJsonFromCsv("src/data/matches.csv");
let json_2 = csvToJson.getJsonFromCsv("src/data/matches.csv");

// pushing values to get matches array of object
let matches_arr=[]
for(let i= 0; i< json_2.length ; i++){
    matches_arr.push(json_2[i]);
}

module.exports = {matches_arr,deliveries_arr};
